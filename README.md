# Emotion Recognition

Source for the V4Design of the emotion recognition from paintings.

In order to run the emotion recognition script for paintings, we used
Python 3.7, PyCharm IDE(version 193.6911.25) and the environment settings in Env_(1-5).png.

Inside Emotions_predictor.py you'll need to provide the paths to the dataset/images, the model and the csvs' where the output information will be displayed.

The model is provided in :
https://drive.google.com/file/d/1MXaY1Hu_FBh2L5E4lWDniQW6mo7Ig8_h/view?usp=sharing
