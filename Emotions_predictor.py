# This code uses k-fold cross validation to train VGG16 to a given dataset

# necessary imports
from keras.applications.imagenet_utils import preprocess_input
import csv
import datetime
import pickle
import h5py
import numpy as np
import os
import keras
import pytz
from natsort import natsort
import pandas as pd
from keras.models import load_model
from keras import backend as K
from keras.preprocessing import image
import json

# functions used in order to track all these metrics while training phase
def recall_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall

def precision_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision

def f1_m(y_true, y_pred):
    precision = precision_m(y_true, y_pred)
    recall = recall_m(y_true, y_pred)
    return 2 * ((precision * recall) / (precision + recall + K.epsilon()))

class Header(dict):
    def __init__(self, header, timestamp, body):
        dict.__init__(self, header=header, timestamp=timestamp, body=body)

class Body(dict):
    def __init__(self, simmo, frame, style, style_probability):
        dict.__init__(self, simmo=simmo, frame=frame, style=style, style_probability=style_probability)


dataset = 'D:/Codes/STBOL_new/testest/'                   # <<<--- INSERT DATASET

images = os.listdir(dataset)          # path4
images = natsort.natsorted(images)
print(images)

custom_objects = {
    'f1_m': f1_m,
    'precision_m': precision_m,
    'recall_m': recall_m
}

model = load_model('VGG19_arch_sgd{0.001, 256, 130}_styles__split_3.hdf5',
                   custom_objects=custom_objects)
print('Model loaded')

predictions = []

with open('D:/Codes/training/Emotion_analysis.csv', 'w') as f:
    thewriter = csv.writer(f)
    thewriter.writerow(['simmo', 'frame', 'emotion', 'emotion_probability'])
    for index, frame in enumerate(images):  # or db_data --> check produced frames in jsons etc
        pred_arc = {}
        img_path = dataset + images[index]        ##testtest, path4
        img = image.load_img(img_path, target_size=(224, 224))
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0)
        x = preprocess_input(x, mode='caffe')
        pred_arc['frame'] = frame
        prediction = model.predict(x)
        pred_arc['pred'] = np.amax(prediction)
        pred_arc['emotion'] = np.argmax(prediction)
        predictions.append(pred_arc)

        b = images[index]
        n = np.amax(prediction)
        q = np.argmax(prediction)
        g = str(img_path)
        thewriter.writerow([b, g, q, n])


LL_emotion_classes = pd.read_csv('D:/Codes/training/LL_emotion.csv', header=None, sep=",")
text = pd.read_csv('D:/Codes/training/Emotion_analysis.csv', sep=",")


with open('D:/Codes/training/Emotion_analysis_final.csv', 'w') as f:
    thewriter = csv.writer(f)
    thewriter.writerow(['simmo', 'frame', 'emotion', 'emotion_probability'])
    for i in range(0, len(text)):
        b = text['simmo'][i]  #### image class - ground truth
        needed_frame = text['frame'][i].split("/")[-1]
        h = needed_frame
        n = text['emotion'][i]
        q = text['emotion_probability'][i]
        z = LL_emotion_classes[1][n]
        ff = z
        thewriter.writerow([b, h, ff, q])
print("CSVs writen")

saving_list = []

for i in range(0, len(text)):
    needed_frame = text['frame'][i].split("/")[-1]
    h = needed_frame
    n = text['emotion'][i]
    q = text['emotion_probability'][i]

    z = LL_emotion_classes[1][n]
    ff = z
    json_body = Body(simmo=text['simmo'][i], frame=needed_frame, style=ff,
                     style_probability=float(q))
    json_header = Header(header="Emotion_recognition",
                         timestamp=str(datetime.datetime.now(tz=pytz.timezone('CET'))) + " CET",
                         body=json_body)
    saving_list.append(json_header)

with open('D:/Codes/training/Emotion_analysis.json', 'w') as arch_json:
    saving_list = str(saving_list)
    arch_json.write(saving_list)




